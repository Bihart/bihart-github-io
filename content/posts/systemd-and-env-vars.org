#+title: Things Last as Long as They Have To
#+date: 2025-01-06T16:39:44-05:00
#+tags[]: software systemd environment-variables linux gnu/linux
#+draft: true
** TL;DR

#+begin_src sh
  systemctl --user daemon-reload
  systemctl --user restart emacs-desktop.service
#+end_src

*** Resources
